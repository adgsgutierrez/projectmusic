package co.com.konrad.myapplication;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailMusic extends AppCompatActivity {

    private ImageView logoartista;
    private TextView cancion1;
    private TextView cancion2;
    private TextView cancion3;
    private TextView cancion4;
    private TextView cancion5;
    private TextView cancion6;
    private TextView cancion7;
    private TextView cancion8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_music);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        logoartista = (ImageView) findViewById(R.id.logoArtista);
        cancion1 = (TextView) findViewById(R.id.textView1);
        cancion2 = (TextView) findViewById(R.id.textView2);
        cancion3 = (TextView) findViewById(R.id.textView3);
        cancion4 = (TextView) findViewById(R.id.textView4);
        cancion5 = (TextView) findViewById(R.id.textView5);
        cancion6 = (TextView) findViewById(R.id.textView6);
        cancion7 = (TextView) findViewById(R.id.textView7);
        cancion8 = (TextView) findViewById(R.id.textView8);
        String artista = "";
        Integer value = intent.getIntExtra("btn" , 0);
        switch (value){
            case 1:
                logoartista.setImageResource(R.mipmap.charly);
                artista = "Charly";
                break;
            case 2:
                logoartista.setImageResource(R.mipmap.daft);
                artista = "Daft Punk";
                break;
            case 3:
                logoartista.setImageResource(R.mipmap.chris);
                artista = "Chris";
                break;
            case 4:
                logoartista.setImageResource(R.mipmap.iyou);
                artista = "I miss You";
                break;
            case 5:
                logoartista.setImageResource(R.mipmap.michael);
                artista = "Michael Jasson";
                break;
            case 6:
                logoartista.setImageResource(R.mipmap.morrons);
                artista = "Morrons";
                break;
            default:
                logoartista.setImageResource(R.mipmap.music);
                artista = "DEMO";
                break;
        }
        cancion1.setText("Canción 1 del álbum de "+ artista);
        cancion2.setText("Canción 2 del álbum de "+ artista);
        cancion3.setText("Canción 3 del álbum de "+ artista);
        cancion4.setText("Canción 4 del álbum de "+ artista);
        cancion5.setText("Canción 5 del álbum de "+ artista);
        cancion6.setText("Canción 6 del álbum de "+ artista);
        cancion7.setText("Canción 7 del álbum de "+ artista);
        cancion8.setText("Canción 8 del álbum de "+ artista);
    }

}
